using System;
using Representorer;

namespace Resourcerer.ResourceModel
{
    public class MethodArguments
    {
        public MethodArguments(Tuple<UrlPart, object>[] args)
        {
            Args = args;
        }

        public Tuple<UrlPart, object>[] Args { get; }
    }
}