using System;
using System.Threading.Tasks;
using HyperMapper.ResourceModel;
using OneOf;
using Resourcerer.ResourceModel;

namespace HyperMapper.RequestHandling
{
    public delegate Task<OneOf<ModelBindingFailed, MethodArguments>> BindModel(MethodParameter[] methodParameters);
}