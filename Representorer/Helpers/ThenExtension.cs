using System;

namespace Representorer.Helpers
{
    public static class ThenExtension
    {
        public static T Then<T>(this T t, Action<T> action)
        {
            action(t);
            return t;
        }

    }
}