using System;

namespace Representorer.Helpers
{
    static class ForEachExtension
    {
        public static void ForEach<T>(this T[] items, Action<T> action)
        {
            foreach (var item in items)
            {
                action(item);
            }
        }
    }
}