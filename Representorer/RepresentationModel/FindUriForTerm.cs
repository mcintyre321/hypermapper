using System;
using Representorer.Vocab;

namespace Representorer.RepresentationModel
{
    public delegate Uri FindUriForTerm(Term term);
}