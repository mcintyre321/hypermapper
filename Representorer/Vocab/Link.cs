using System;
using Newtonsoft.Json.Linq;
using Representorer.RepresentationModel;

namespace Representorer.Vocab
{
    public class Links
    {
        public static SemanticPropertiesSet CreateLink(string parentTitle, Uri parentUri, Term term)
        {

            var properties = new SemanticPropertiesSet
            {
                SemanticProperty.CreateValue<Href>((JToken) parentUri.ToString()),
                SemanticProperty.CreateValue<DisplayText>((JToken) parentTitle),
                SemanticProperty.CreateTerm<Rel>(term)
            };
            return properties;
        }
        public class Href { }
        public class Rel { }

    }


}