using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HyperMapper.ResourceModel;
using Representorer.RepresentationModel;
using Resourcerer.RequestHandling;
using Resourcerer.ResourceModel;

namespace Representorer
{
    public abstract class Representor 
    {
        public abstract MethodArguments[] Bind(MethodParameter[] parameters, Request request);
        public abstract Task<Representation> GetResponse(Request request, FindUriForTerm termUriFinder);

        public abstract bool CanRespondTo(Request request);
        public abstract bool CanTransform(object doc);
        public abstract Representation BuildRepresentation(Request request, object doc);
        public abstract bool CanBindFrom(Request request);
    }

    
}