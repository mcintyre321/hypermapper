using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Resourcerer.RequestHandling;

namespace Resourcerer.ResourceModel
{
    public class MethodHandler 
    {
        public delegate Task<Response> InvokeMethodDelegate(Request request);
        private readonly InvokeMethodDelegate _invoke;

        public MethodHandler(Method method, InvokeMethodDelegate invoke)
        {
            _invoke = invoke;
            Method = method;
        }

        public Method Method { get; }

        public Task<Response> Invoke(Request request)
        {
            return _invoke(request);
        }
    }
}