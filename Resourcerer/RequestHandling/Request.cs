using System;
using Resourcerer.ResourceModel;

namespace Resourcerer.RequestHandling
{
    public class Request
    {
        public Request(Method method, Uri uri)
        {
            Method = method;
            Uri = uri;
        }

        public Method Method { get; }
        public Uri Uri { get; }
    }
}