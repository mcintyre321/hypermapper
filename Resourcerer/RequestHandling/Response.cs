using OneOf;

namespace Resourcerer.RequestHandling
{
    public abstract class Response : OneOfBase<
        Response.MethodNotAllowed,
        Response.NotFoundResponse,
        Response.ModelBindingFailedResponse,
        Response.SuccessResponse,
        Response.CreatedResponse
        >
    {
        public class MethodNotAllowed : Response  { }
        public class NotFoundResponse : Response { }
        public class ModelBindingFailedResponse : Response { }
        public class SuccessResponse : Response 
        {
            public Representation Representation { get; }

            public SuccessResponse(Representation representation)
            {
                Representation = representation;
            }

        }
        public class CreatedResponse : Response 
        {
            public Representation Representation { get; private set; }

            public CreatedResponse(Representation representation)
            {
                Representation = representation;
            }
        }

    }
}