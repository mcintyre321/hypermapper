using System;

namespace Resourcerer.RequestHandling
{
    public class Representation
    {
        public Func<string> MediaType { get; set; }
        public Func<string> Content { get; set; }

    }
}