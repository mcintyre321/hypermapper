using System.Threading.Tasks;
using OneOf;
using OneOf.Types;
using Resourcerer.ResourceModel;

namespace Resourcerer.RequestHandling
{
    public delegate Task<OneOf<Resource, None>> Router(BaseUrlRelativePath path);
}