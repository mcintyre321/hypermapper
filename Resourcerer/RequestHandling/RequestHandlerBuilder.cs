using System;
using System.Threading.Tasks;
using OneOf;
using OneOf.Types;
using Resourcerer.ResourceModel;

namespace Resourcerer.RequestHandling
{
    public class RequestHandlerBuilder<TRep>
    {
        public RequestHandler MakeRequestHandler(Uri baseUri, Router router)
        {
            return async (request) =>
            {

                var requestUri = new Uri(request.Uri.PathAndQuery.ToString(), UriKind.Relative);

                var path = new BaseUrlRelativePath(requestUri.ToString().Substring(baseUri.ToString().Length));
                var routingResult = await router(path);

                return await routingResult.Match(
                    resource => resource.GetMethodHandler(request.Method)
                        .Match(async handler =>
                            {
                                return await handler.Invoke(request);
                                //var bindResult = await modelBinder(handler.Parameters);
                                //var response = await bindResult.Match(
                                //    failed => Task.FromResult<Response<TRep>>(new Response<TRep>.ModelBindingFailedResponse()),
                                //    async boundModel => (await handler.Invoke(boundModel)).Match(
                                //        representation =>
                                //            (Response<TRep>) new Response<TRep>.SuccessResponse(representation.Representation)
                                //    ));
                                //return response;
                            },
                            none => Task.FromResult((Response) new Response.MethodNotAllowed())),
                    none => Task.FromResult((Response) new Response.NotFoundResponse()));
            };
        }
    }
}