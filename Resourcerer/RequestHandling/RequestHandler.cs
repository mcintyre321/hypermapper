using System.Threading.Tasks;

namespace Resourcerer.RequestHandling
{
    public delegate Task<Response> RequestHandler(Request request);
}