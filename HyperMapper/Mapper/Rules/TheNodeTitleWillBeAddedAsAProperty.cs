using System;
using HyperMapper.Mapping;
using Newtonsoft.Json.Linq;
using Representorer.RepresentationModel;
using Representorer.Vocab;

namespace HyperMapper.Mapper.Rules
{
    public class TheNodeTitleWillBeAddedAsAProperty : Functions.IAbstractNodeToSemanticDocumentMappingRule
    {
        public void Apply(AbstractNode node, SemanticDocument doc)
        {
            doc.Value.Add(SemanticProperty.CreateValue(Terms.Title, JToken.FromObject(node.Title)));

        }
    }
}