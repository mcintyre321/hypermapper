using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using HyperMapper.Mapper.Rules;
using HyperMapper.Mapping;
using HyperMapper.Mapping.ResultTypes;
using HyperMapper.ResourceModel;
using Newtonsoft.Json.Linq;
using Representorer;
using Representorer.RepresentationModel;
using Representorer.Vocab;
using Resourcerer;
using Resourcerer.RequestHandling;
using Resourcerer.ResourceModel;

namespace HyperMapper.Mapper
{
    public class Functions
    {
        public interface IAbstractNodeToSemanticDocumentMappingRule
        {
            void Apply(AbstractNode abstractNode, SemanticDocument doc);
        }

        public static Resource MakeResourceFromNode(AbstractNode node, ServiceLocatorDelegate serviceLocator,
            Representor[] representors)
        {
            var rules = new List<IAbstractNodeToSemanticDocumentMappingRule>()
            {
                new TheNodeTitleWillBeAddedAsAProperty(),
                new ThereWillBeALinkToTheParentNodeInTheDocument(),
                new ImplementingIHasChildNodesWillExposeTheItemsAsNodeResources(),
                new ImplementingIHasHyperlinksWillAddALinkPropertyToDocument(),
                new MarkingAPropertyWithExposeAttributeWillAddAValuePropertyToDocument(),
                new MarkingAMethodWithExposeAttributeWillAddTheMethodAsAnActionResource(),
            };

            var methodNode = node as MethodInfoNode;
            if (methodNode != null)
            {
                return new Resource(new[]
                {
                    BuildGetHandlerForMethodInfo(methodNode, representors),
                    BuildPostHandlerForMethodInfo(methodNode, serviceLocator, representors)
                });
            }

            var methodHandlers = new[]
            {
                new MethodHandler(new Method.Get(), request =>
                {
                    var doc = new SemanticDocument();
                    foreach (var mappingRule in rules)
                    {
                        mappingRule.Apply(node, doc);
                    }

                    var representor = representors.First(r => r.CanRespondTo(request) && r.CanTransform(doc));
                    var representation = representor.BuildRepresentation(request, doc);
                    return Task.FromResult<Response>(new Response.SuccessResponse(representation));
                })
            };
            return new Resource(methodHandlers.ToArray());
        }


        private static MethodHandler BuildGetHandlerForMethodInfo(MethodInfoNode methodInfoNode,
            Representor[] representors)
        {
            return new MethodHandler(new Method.Get(), request =>
            {
                var doc = new SemanticDocument();
                doc.Value.Add(SemanticProperty.CreateValue(Terms.Title, JToken.FromObject(methodInfoNode.Title)));
                AddSemanticItemsFromMethodInfo(methodInfoNode, doc);
                var representor = representors.First(r => r.CanRespondTo(request) && r.CanTransform(doc));
                var representationResult = representor.BuildRepresentation(request, doc);
                return Task.FromResult<Response>(new Response.SuccessResponse(representationResult));
            });
        }


        private static void AddSemanticItemsFromMethodInfo(MethodInfoNode methodInfoNode,
            SemanticDocument representation)
        {

            var term = TermFactory.From(methodInfoNode.MethodInfo);
            var methodParameters = methodInfoNode.GetParameters();
            var operation = Operation.Create(methodInfoNode.Title, methodParameters, methodInfoNode.Uri, term);
            representation.Value.Add(operation);

            representation.AddLink(Links.CreateLink(methodInfoNode.Parent.Title, methodInfoNode.Parent.Uri,
                Terms.Parent));
        }


        private static MethodHandler BuildPostHandlerForMethodInfo(MethodInfoNode methodNode,
            ServiceLocatorDelegate serviceLocator, Representor[] representors)
        {
            var invokeResult = null as InvokeResult<>
            //MethodHandler.InvokeMethodDelegate invoke2 = async (request) =>
            //{
            //    var representor = representors.First(r => r.CanBindFrom(request));

            //    var parameterInfo = methodNode.GetParameters();

            //    var args = representor.Bind(parameterInfo, request);
            //    var argsList = parameterInfo
            //        .Select(pi =>
            //        {
            //            if (pi.GetCustomAttribute<InjectAttribute>() != null)
            //            {
            //                if (serviceLocator == null)
            //                {
            //                    throw new InvalidOperationException(
            //                        $"Cannot [Inject] parameter {pi.Name} for {methodNode.MethodInfo.DeclaringType.Name}.{methodNode.MethodInfo.DeclaringType.Name} Please set ServiceLocator at startup");
            //                }
            //                return serviceLocator(pi.ParameterType);
            //            }
            //            else
            //            {
            //                argsEnumerator.MoveNext();
            //                var current = argsEnumerator.Current;
            //                if (current.Item1 != new UrlPart(pi.Name))
            //                    throw new ArgumentException("Mismatch: expected " + pi.Name + ", received" +
            //                                                current.Item1.ToString());
            //                return Tuple.Create(current.Item2, null as Action);
            //            }
            //        }).ToList();
            //    var parameters = argsList.Select(a => a.Item1).ToArray();
            //    var invokeResult = methodNode.MethodInfo.Invoke(methodNode.Parent, parameters);
            //    foreach (var tuple in argsList)
            //    {
            //        tuple.Item2?.Invoke();
            //    }

                if (methodNode.MethodInfo.ReturnType == typeof(void))
                {
                }
                var task = invokeResult as Task;
                if (task != null)
                {
                    await task;
                    invokeResult = task.GetType().GetRuntimeProperty("Result")?.GetValue(task) ?? invokeResult;
                }
                var document = new SemanticDocument();


                //Add a link back to the thing that the action happened to 
                {
                    OneOf.OneOf<Modified, Created> result = default(OneOf.OneOf<Modified, Created>);
                    if (invokeResult is Created) result = (Created)invokeResult;
                    if (invokeResult is Modified) result = (Modified)invokeResult;
                    if (invokeResult is AbstractNode) result = new Created((AbstractNode)invokeResult);
                    result = new Modified(methodNode.Parent);

                    result.Switch(
                        modified => document.AddLink(Links.CreateLink($"Modified \'{modified.Node.Title}\'",
                            modified.Node.Uri, modified.Node.Term)),
                        created => document.AddLink(Links.CreateLink($"Created \'{created.Node.Title}\'",
                            created.Node.Uri, created.Node.Term)));
                }

                document.Value.Add(SemanticProperty.CreateValue(Terms.Title, JToken.FromObject(methodNode.Title)));
                return new Response.SuccessResponse(document);
            };


            return new MethodHandler<SemanticDocument>(new Method.Post(), invoke2);
        }
    }
}